# Apertis Development Environment

Apertis Development Environment (ADE) is a tool that helps developpers to
manage sysroots, cross-compile applications, deploy them to target and
then debug them.

See the man page for more details about the available commands and subcommands.

## import-debian-package

`import-debian-package` script imports a package from Debian in the format
expected by the Apertis CI system.

To import a new package from Debian the previous process required several manual
steps which are now performed automatically by the script using the
`--push-remote` option.

The following steps are now automatized:
* package folder is created and import process is performed in there.
* target debian package is fetched and imported.
* remote repo is created if it doesn't exist.
  * Create an empty project on GitLab under the `pkg` namespace (for instance,
    `pkg/hello`). Choose visibility level `Public` when creating the project.
  * Configure the origin remote on your local git:
```
$ git remote add origin git@gitlab.apertis.org:pkg/hello
```
* remote branches and tags are pushed.
```
$ git push --all --follow-tags origin
```
* gitlab settings are applied to remote repo using `gitlab-rulez`:
  * sets the CI config path to
    `ci-package-builder.yml@infrastructure/ci-package-builder`
  * changes the merge request settings to:
    * only allow fast-forward merges
    * ensure merges are only allowed if pipelines succeed
  * marks the `apertis/*` and `debian/*` branches as protected
```
$ gitlab-rulez apply ../apertis-infrastructure/gitlab-scripts/rulez.yaml --filter pkg/hello
```
* CI pipeline is triggered on all downstream branches.
