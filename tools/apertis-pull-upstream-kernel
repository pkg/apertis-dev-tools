#!/bin/bash

# This script is used to rebase the apertis kernel on the latest upstream kernel.
# See more information there:
# https://www.apertis.org/guides/buildingpatchingandmaintainingtheapertiskernel/#updating-the-kernel-from-an-upstream-stable-git-branch
#
# The apertis linux sources git local repository must have a apertis/v.*-security branch checked
# out (e.g.: apertis/v2022-security).
#
# Arguments
# $1: Path to apertis linux sources

set -xe

LINUX_STABLE_DIR=$(mktemp -d)

# Print script usage
print_usage() {
    echo "Usage:"
    echo "  $0 <PATH_TO_LINUX_SOURCES>"
}

# Fetch the latest version of the upstream release branch
# args:
#   $1: Upstream kernel release
fetch_upstream_remote() {
    local UPSTREAM_RELEASE="$1"

    git clone \
        -b "linux-${UPSTREAM_RELEASE}.y" \
        --depth=1 \
        git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git \
        ${LINUX_STABLE_DIR}
}

# Import the orig tarball and rebase patches on it
# args:
#   $1: Upstream kernel release (e.g.: 5.15)
#   $2: Upstream kernel version (e.g.: v5.15.42)
#   $3: Apertis release suite (e.g.: v2022)
import_orig_tarball() {
    local UPSTREAM_RELEASE="$1"
    local UPSTREAM_VERSION="$2"
    local APERTIS_RELEASE="$3"
    local APERTIS_VERSION="${UPSTREAM_VERSION#v}-0+apertis1"

    dch -v "${APERTIS_VERSION}" ""
    git add -f debian/changelog
    git commit -s -m "Add changelog entry for update to ${APERTIS_VERSION}"

    debian/bin/genorig.py ${LINUX_STABLE_DIR}

    git fetch --depth=1 origin upstream/linux-${UPSTREAM_RELEASE}.y
    git branch \
        upstream/linux-${UPSTREAM_RELEASE}.y \
        origin/upstream/linux-${UPSTREAM_RELEASE}.y

    gbp import-orig \
        -u ${UPSTREAM_VERSION#v} \
        --upstream-branch=upstream/linux-${UPSTREAM_RELEASE}.y \
        --debian-branch=apertis/${APERTIS_RELEASE}-security \
        ../orig/linux_${UPSTREAM_VERSION#v}.orig.tar.xz

    git push \
        ${CI_AUTH_PROJECT_URL} \
        upstream/linux-${UPSTREAM_RELEASE}.y

    git tag upstream/${UPSTREAM_VERSION#v}
    git push ${CI_AUTH_PROJECT_URL} upstream/${UPSTREAM_VERSION#v}
}

cleanup() {
    # Remove temporary folders
    rm -fr "${LINUX_STABLE_DIR}"

    # Go back to the previous folder
    popd
}

trap cleanup EXIT

if [ -z "$1" ]; then
    echo "Path to apertis linux sources must be provided"
    print_usage
    exit 1
fi
LINUX_SOURCES="$1"

pushd ${LINUX_SOURCES}

APERTIS_RELEASE=$(git branch --show-current | sed 's/apertis\/\(v.*\)-security$/\1/')
if [ -z "${APERTIS_RELEASE}" ]; then
    echo "Cannot get apertis release suite. Check that you are running this pipeline on an apertis/v*-security branch."
    exit 1
fi

# Get upstream release (e.g. 5.15)
UPSTREAM_RELEASE=$(git describe | sed 's#apertis/\([0-9]\+\.[0-9]\+\)\..*#\1#')
if ! echo ${UPSTREAM_RELEASE} | grep -q '^[0-9]\+\.[0-9]\+$'; then
    echo "Cannot get upstream release. Check that you are running this pipeline on an apertis/v*-security branch."
    exit 1
fi

fetch_upstream_remote "${UPSTREAM_RELEASE}"

UPSTREAM_VERSION=$(git -C ${LINUX_STABLE_DIR} describe linux-${UPSTREAM_RELEASE}.y)

import_orig_tarball \
    ${UPSTREAM_RELEASE} \
    ${UPSTREAM_VERSION} \
    ${APERTIS_RELEASE}
