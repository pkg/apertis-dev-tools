# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.
# 2022 Collabora, Ltd.

#[=======================================================================[.rst:
GenerateABIDescriptor
-------

Generate an XML library descriptor file to be used
by abi-compliance-checker and apertis-abi-compare.

find_package(GenerateABIDescriptor REQUIRED)
generate_abi_descriptor(${PROJECT_NAME}
                        "DEFAULT"
                        "DEFAULT"
                        )

#]=======================================================================]

include(GNUInstallDirs)

function(generate_abi_descriptor LIB_NAME HEADER_PATH SHARED_PATH)

    get_target_property(VER ${LIB_NAME} VERSION)
    get_target_property(PUBLIC_HEADER ${LIB_NAME} PUBLIC_HEADER)

    message("[abi_descriptor_generator] Generating a descriptor for ${LIB_NAME} ${VER}")

    set(XML_NAME "lib${LIB_NAME}-abi-descriptor.xml")

    if(HEADER_PATH STREQUAL "DEFAULT")
        message("[abi_descriptor_generator] HEADER_PATH is not defined!")
        message("[abi_descriptor_generator]   Use the default header install location instead")
        set(HDRS_PATH "/usr/include/${PROJECT_NAME}/")
    else()
        message("[abi_descriptor_generator] Use the defined HEADER_PATH")
        set(HDRS_PATH "${HEADER_PATH}")
    endif()


    message("[abi_descriptor_generator] Use public headers:")
    set(HEADER_LIST)
    foreach(HDR ${PUBLIC_HEADER})
        set(HDR_PATH "    \${PATH\}${HDRS_PATH}${HDR}")
        message("[abi_descriptor_generator] ${HDR_PATH}")
        list(APPEND HEADER_LIST "${HDR_PATH}")
    endforeach()
    string(REPLACE ";" "\n" HDR_LIST "${HEADER_LIST}")

    set(ABI_HEADER ${HDR_LIST})

    if(SHARED_PATH STREQUAL "DEFAULT")
        message("[abi_descriptor_generator] SHARED_PATH is not defined!")
        message("[abi_descriptor_generator]   Use the default library install location instead")
        set(ABI_SHARED "    \${PATH\}${CMAKE_INSTALL_FULL_LIBDIR}/lib${LIB_NAME}.so")
    else()
        message("[abi_descriptor_generator] Use the defined SHARED_PATH")
        set(ABI_SHARED "    \${PATH\}${SHARED_PATH}/lib${LIB_NAME}.so")
    endif()

    message("[abi_descriptor_generator] Use shared library:")
    message("[abi_descriptor_generator] ${ABI_SHARED}")

    configure_file("/usr/lib/cmake/GenerateABIDescriptor/GenerateABIDescriptor.xml.in" "${XML_NAME}" @ONLY)

    install(FILES "${CMAKE_CURRENT_BINARY_DIR}/${XML_NAME}"
            DESTINATION "/usr/share/doc/lib${LIB_NAME}-dev/" )

endfunction()
