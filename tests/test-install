#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

import os
import tempfile

from test_util import SysrootServer
from test_util import templatedconfig
from test_util import should_succeed, should_fail
from test_util import add_auth_params, create_temp_netrc
from test_util import BASE_TAG, BASE_ARCHIVE, BASE_CONFIG, \
                      SYSROOTS, LATEST_SYSROOTS, SYSROOT1, SYSROOT2, \
                      CONFIG_FILES, BAD_URLS

# Utility functions
def check_result(result, sysroot):
    expected_tag = BASE_TAG.format(*sysroot)
    return result['InstalledVersion'] == expected_tag

def check_install(path, sysroot):
    expected_version = "{0} {1} {3} collabora".format(*sysroot)
    with open(os.path.join(path, sysroot[0], sysroot[1], sysroot[2], 'etc', 'image_version')) as f:
        result_version = f.read().strip()
    return result_version == expected_version

def check_sysroot(result, path, sysroot):
    return check_result(result, sysroot) and check_install(path, sysroot)

# Setup
server = SysrootServer()
server.start()

# Test "ade sysroot install --url" usage
for sysroot in SYSROOTS:
    with tempfile.TemporaryDirectory() as tmpdir:
        params = ['--url', server.base_url().format(*sysroot), '--distro', sysroot[0],
                  '--release', sysroot[1], '--arch', sysroot[2]]
        add_auth_params(params, sysroot)
        should_succeed('sysroot', '--path', tmpdir, 'install', *params,
                       check=lambda x: check_sysroot(x, tmpdir, sysroot))

# Test "ade sysroot install --url" usage with user and password in url
for sysroot in SYSROOTS:
    with tempfile.TemporaryDirectory() as tmpdir:
        params = ['--url', server.base_url_with_auth().format(*sysroot), '--distro', sysroot[0],
                  '--release', sysroot[1], '--arch', sysroot[2]]
        should_succeed('sysroot', '--path', tmpdir, 'install', *params,
                       check=lambda x: check_sysroot(x, tmpdir, sysroot))

# Test "ade sysroot install --url" usage with user and password in netrc file
temp_netrc = create_temp_netrc(server)
for sysroot in SYSROOTS:
    with tempfile.TemporaryDirectory() as tmpdir:
        params = ['--url', server.base_url().format(*sysroot), '--distro', sysroot[0],
                  '--release', sysroot[1], '--arch', sysroot[2], '--netrc', temp_netrc.name]
        should_succeed('sysroot', '--path', tmpdir, 'install', *params,
                       check=lambda x: check_sysroot(x, tmpdir, sysroot))
temp_netrc.close()

# Test "ade sysroot install --file" usage
for sysroot in SYSROOTS:
    with tempfile.TemporaryDirectory() as tmpdir:
        should_succeed('sysroot', '--path', tmpdir, 'install', '--file', BASE_ARCHIVE.format(*sysroot),
                       check=lambda x: check_sysroot(x, tmpdir, sysroot))

# Test "ade sysroot install --distro --release --arch" usage
for sysroot in SYSROOTS:
    with tempfile.TemporaryDirectory() as tmpdir:
        with templatedconfig(server, BASE_CONFIG.format(*sysroot)) as config:
            params = ['--distro', sysroot[0], '--release', sysroot[1], '--arch', sysroot[2]]
            add_auth_params(params, sysroot)
            should_succeed('sysroot', '--config', config, '--path', tmpdir, 'install', *params,
                           check=lambda x: check_sysroot(x, tmpdir, sysroot))

# Test "ade sysroot install --distro --release --arch" usage with different config files
for config_file in CONFIG_FILES:
    with templatedconfig(server, config_file) as config:
        for sysroot in LATEST_SYSROOTS:
            with tempfile.TemporaryDirectory() as tmpdir:
                params = ['--distro', sysroot[0], '--release', sysroot[1], '--arch', sysroot[2]]
                add_auth_params(params, sysroot)
                should_succeed('sysroot', '--config', config, '--path', tmpdir, 'install', *params,
                               check=lambda x: check_sysroot(x, tmpdir, sysroot))

# Test "ade sysroot install --force" usage
with tempfile.TemporaryDirectory() as tmpdir:
    should_succeed('sysroot', '--path', tmpdir, 'install', '--file', BASE_ARCHIVE.format(*SYSROOT1),
                   check=lambda x: check_sysroot(x, tmpdir, SYSROOT1))

    # Shouldn't let the user install same sysroot (apertis 16.09 - armhf)
    should_fail('sysroot', '--path', tmpdir, 'install', '--file', BASE_ARCHIVE.format(*SYSROOT1),
                check=lambda x: check_install(tmpdir, SYSROOT1))
    should_fail('sysroot', '--path', tmpdir, 'install', '--file', BASE_ARCHIVE.format(*SYSROOT2),
                check=lambda x: check_install(tmpdir, SYSROOT1))

    # Force install
    should_succeed('sysroot', '--path', tmpdir, 'install', '--force', '--file', BASE_ARCHIVE.format(*SYSROOT2),
                   check=lambda x: check_sysroot(x, tmpdir, SYSROOT2))

# Test error cases
with tempfile.TemporaryDirectory() as tmpdir:
    for url in BAD_URLS:
        params = ['--url', url, '--distro', 'eraroj', '--release', '16.09', '--arch', 'armhf']
        should_fail('sysroot', '--path', tmpdir, 'install', *params)

# Tear down
server.stop()
