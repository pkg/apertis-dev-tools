#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

import os

from test_util import should_succeed, should_fail, split_elements
from test_util import BASE_INSTALL, BASE_TAG, LATEST_SYSROOTS

# Utility functions
def check_sysroot(result, sysroot):
    expected = BASE_TAG.format(*sysroot)
    return result['InstalledVersion'] == expected

def check_list(result, expected_sysroots):
    result_sysroots = split_elements(result['InstalledSysroots'])
    if len(expected_sysroots) != len(result_sysroots):
        return False
    if not expected_sysroots:
        return True
    expected_versions = [BASE_TAG.format(*x) for x in sorted(expected_sysroots)]
    for (expected, result) in zip(expected_versions, result_sysroots):
        if expected != result:
            return False
    return True

# Test "ade installed" usage
for sysroot in LATEST_SYSROOTS:
    params = ['--distro', sysroot[0], '--release', sysroot[1], '--arch', sysroot[2]]
    should_succeed('sysroot', '--path', BASE_INSTALL, 'installed', *params,
                   check=lambda x: check_sysroot(x, sysroot))
    tag = '{}-{}-{}'.format(*sysroot)
    should_succeed('info', '--path', BASE_INSTALL, '--sysroot', tag)

# Test "ade list" usage
should_succeed('sysroot', '--path', BASE_INSTALL, 'list',
               check=lambda x: check_list(x, LATEST_SYSROOTS))

# Empty directory
should_succeed('sysroot', '--path', os.path.join(os.getcwd(), 'missing'),
               'installed', '--distro', 'eraroj', '--release', '16.09',
               check=lambda x: not x)
should_succeed('sysroot', '--path', os.path.join(os.getcwd(), 'missing'), 'list',
               check=lambda x: check_list(x, []))
