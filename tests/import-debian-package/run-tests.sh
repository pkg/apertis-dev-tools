#!/bin/sh
# vim: set sts=4 sw=4 et :

set -e

export TESTBASE=$(dirname $(realpath $0))
export BASE=$TESTBASE/../../tools

cd "$TESTBASE"

. ./common.sh

export DEBUG=1

# Prepare sane environment
PATH=$TESTBASE:/usr/sbin:/usr/bin:/sbin:/bin
HOME=$BASEWORKDIR

###########
# Execute #
###########
trap "test_failure" EXIT

src_test_pass <<-EOF
01-basic
02-basic-no-suffix
03-exact
04-php
05-local-dsc-import
EOF

test_success
